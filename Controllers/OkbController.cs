﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Text;

namespace NBCH.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OkbController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public OkbController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpPost]
        public async void OkbList()
        {
            DateTime startEmul = DateTime.Now;
            string id = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString();

            string xmlData = "";
            if (Request.Body != null)
            {
                var streamReader = new StreamReader(Request.Body);
                xmlData = await streamReader.ReadToEndAsync();
            }

            if (bool.Parse(Configuration["RKK:Okb:logRqRsMessage"]))
            {
                string path = @"C:\log\RKK";
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                using (FileStream stream = new FileStream($"{path}\\" + id + "_request.xml", FileMode.OpenOrCreate))
                {
                    byte[] array = Encoding.Default.GetBytes(xmlData);
                    stream.Write(array, 0, array.Length);
                }
            }

        }
        //[HttpGet]
        //public void OkbList()
        //{
        //    string[] files = Directory.GetFiles(@"Files\Okb");

        //    List<string> nameFiles = new List<string>();
        //    foreach (string s in files)
        //    {
        //        string[] words = s.Split(new char[] { '\\' });
        //        nameFiles.Add(words[2]);
        //    }

        //    nameFiles.ToArray();

        //    string str = "";
        //    for (int i=0; i < nameFiles.Count; i++)
        //    {
        //        str += nameFiles[i].ToString() + " ";
        //    }

        //    Response.WriteAsync(str);
        //}

        //[HttpGet]
        //public FileContentResult OkbFile(string file)
        //{
        //    byte[] arrayRead;
        //    using (FileStream fstream = System.IO.File.OpenRead("Files/Okb/" + file))
        //    {
        //        arrayRead = new byte[fstream.Length];
        //        fstream.Read(arrayRead, 0, arrayRead.Length);
        //    }

        //    FileContentResult fileContent = new FileContentResult(arrayRead, "multipart/form-data");

        //    return fileContent;
        //}
    }
}