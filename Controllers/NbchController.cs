﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace NBCH.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NbchController : ControllerBase
    {
        private readonly IConfiguration Configuration;

        public NbchController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        [HttpGet]
        public FileContentResult NbchGet()
        {
            string[] files = Directory.GetFiles(Configuration["RKK:Nbch:PathOut"]);

            DateTime date = DateTime.Now;
            string stringDate = date.ToString("yyyyMMdd") + "_" + date.ToString("hhmmss") + "_Reply.xml";

            System.IO.File.Move(files[0], files[0].Remove(files[0].IndexOf('_') + 1) + stringDate);
            
            string[] filesNew = Directory.GetFiles(Configuration["RKK:Nbch:PathOut"]);

            byte[] arrayRead;
            using (FileStream fstream = System.IO.File.OpenRead(filesNew[0]))
            {
                arrayRead = new byte[fstream.Length];
                fstream.Read(arrayRead, 0, arrayRead.Length);
            }

            FileContentResult fileContent = new FileContentResult(arrayRead, "multipart/form-data");

            System.IO.File.Delete(filesNew[0]);

            return fileContent;
        }

        [HttpPost]
        public void NbchPost()
        {
            try
            {
                var file = Request.Form.Files[0];
                string path = Configuration["RKK:Nbch:PathIn"];

                // записываем пришедший файл на винт
                using (var fileStream = new FileStream(path + file.Name, FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }
            catch (Exception e)
            {
                Response.WriteAsync(e.Message);
            }
        }
    }
}